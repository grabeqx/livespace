/*
    Ten komponent nie działa 😱 Jak trzeba go naprawić? Dlaczego nie działał?
    Co w tym kompnencie można zmienić, żeby był ładniej/sensowniej napisany?
*/

import React, { Component } from 'react'

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: this.props.name || 'Anonymous'
		};
	}

	handleOnChange = event => {
		this.setState({
			name: event.target.value.length ? event.target.value : 'Anonymous'
		});
	};

	render() {
		return (
			<div>
				<input type="text" onChange={this.handleOnChange} />
				<p>Hello {this.state.name}</p>
			</div>
		);
	}
}

export default App;