/*
    Poniższy komponent wygląda, jakby miał działać, ale... nie działa.
    Jak można go naprawić? Dlaczego nie działa?
*/

import React from 'react';

class App extends React.Component {
	state = { search: '' };

	handleChange = event => {
		let value = event.target.value;
		clearTimeout(this.timeout);
		this.timeout = setTimeout(() => {
			this.setState({
				search: value
			});
		}, 250);
	};

	render() {
		return (
			<div>
				<input type="text" onChange={this.handleChange} />
				{this.state.search ? <p>Search for: {this.state.search}</p> : null}
			</div>
		);
	}
}

export default App;
