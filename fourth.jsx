/*
    W poniższym komponencie `console.log('render')` pokazuje się około 100 razy po każdym kliknięciu guzika do usunięcia elementu listy.

    Zachowując podział na komponenty App i ListElement, oraz wygląd i działanie kodu po stronie użytkownika, co można zrobić aby:
    1. Wywołwać jak najmniej przerenderowań? (Czyli ograniczyć liczbę logów 'render' na konsolę, bez usuwania console.log 😄)
    2. Przyśpieszyć działanie tego kodu?

    Czy coś jeszcze można tu poprawić?
*/

import React, { Component } from 'react';

const ListElement = React.memo(props => {
	console.log('render');
	return (
		<li>
			{props.number}. {props.content}
		</li>
	);
});

class App extends React.PureComponent {
	state = {
		// Zignoruj poniższą linijkę - potrzebujemy udawać, że mamy tablicę zapełnioną danymi, nie jest to część zadania.
		elements: new Array(100).fill().map(() => `I'm a list element! ${Math.floor(Math.random() * 1000)}`), // Możesz przyjąć, że wszystkie elmenty tablicy `elements` są unikalne.
		transformedData: []
	};

	componentDidMount() {
		let transformedData = this.state.elements.map((value, index) => ({
			index: index,
			content: value
		}));

		this.setState({
			transformedData: transformedData
		});
	}

	handleClick = () => {
		this.setState({
			transformedData: this.state.transformedData.slice(1)
		});
	};

	render() {
		console.log('render');

		return (
			<div>
				<button onClick={this.handleClick}>Remove first list element</button>
				<ul style={{ listStyle: 'none' }}>
					{this.state.transformedData.map(value => (
						<ListElement key={value.index} content={value.content} number={value.index + 1} />
					))}
				</ul>
			</div>
		);
	}
}

export default App;
