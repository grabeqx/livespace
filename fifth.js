/*
    W reducerze createApp przy obsłudze każdej z akcji w kodzie jest jakiś problem.
    Jak można poprawić każdy z nich? Dlaczego z ich kodem jest coś nie tak?
*/

const initialState = {
	visibilityFilter: 'none',
	products: [
		{
			id: 305,
			price: 100,
			lastSold: '2020-02-12T13:19:15.784Z'
		},
		{
			id: 308,
			price: 500,
			lastSold: '2020-02-14T11:12:18.113Z'
		},
		{
			id: 309,
			price: 299,
			lastSold: '2020-02-14T14:32:06.361Z'
		}
	]
};

function createApp(state = initialState, action) {
	switch (action.type) {
		case SET_VISIBILITY_FILTER:
			return {
				...state,
				visibilityFilter: action.data.filter
			};
		case CHANGE_PRODUCT_PRICE:
			const products = state.products.map((product, index) => {
				if (index === action.data.id) {
					product.price = action.data.newPrice;
				}
				return product;
			});
			return {
				...state,
				products: products
			};
		case UPDATE_LAST_SOLD:
			const products = state.products.map(product => {
				if (index === action.data.id) {
					product.lastSold = new Date().toISOString();
				}
				return product;
			});
			return {
				...state,
				products: products
			};

		default:
			return state;
	}
}
