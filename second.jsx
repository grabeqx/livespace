/*
    Poniższy kod się nie uruchamia i ma w sobie kilka błędów. Jak go naprawić? Co można zmienić w tym kodzie?
*/

import React from 'react';

const Counter = ({ add, count }) => <li>{`${add} plus current count is ${add + count}`}</li>;

class App extends React.Component {
	state = {
		count: 0
	};

	handleClick = () => {
		this.setState(
			{
				count: this.state.count + 1
			},
			() => {
				console.log(100 + this.state.count);
			}
		);
	};

	render() {
		return (
			<div>
				<button onClick={this.handleClick}>count up</button>
				<Counter add={100} count={this.state.count} />
			</div>
		);
	}
}

export default App;
